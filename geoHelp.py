# -*- coding: utf-8 -*-
"""
geoHelp.py , Built on Spyder for Python 2.7
James Watson, 2015 November
Import trigonomtric/geometric functions

"""

# Standard Libraries
import math 
# Standard Names
from math import sqrt, degrees, radians, sin, cos, asin, acos, tan, atan2

# Special Libraries
import matplotlib.pyplot as plt
import numpy as np

